# Graph neural networks

This repo contains three types of graph neural networks implemented with the help of the Deep Java Library(https://djl.ai/)
* Graph Convolutional Networks
* Graph Attention Networks
* Simple Graph Convolution Networks

# Citations

@article{kipf2016semi,
title={Semi-Supervised Classification with Graph Convolutional Networks},
author={Kipf, Thomas N and Welling, Max},
journal={arXiv preprint arXiv:1609.02907},
year={2016}
}

@InProceedings{pmlr-v97-wu19e,
title = 	 {Simplifying Graph Convolutional Networks},
author = 	 {Wu, Felix and Souza, Amauri and Zhang, Tianyi and Fifty, Christopher and Yu, Tao and Weinberger, Kilian},
booktitle = 	 {Proceedings of the 36th International Conference on Machine Learning},
pages = 	 {6861--6871},
year = 	 {2019},
publisher = 	 {PMLR},
}

@misc{Gordić2020PyTorchGAT,
author = {Gordić, Aleksa},
title = {pytorch-GAT},
year = {2020},
publisher = {GitHub},
journal = {GitHub repository},
howpublished = {\url{https://github.com/gordicaleksa/pytorch-GAT}},
}