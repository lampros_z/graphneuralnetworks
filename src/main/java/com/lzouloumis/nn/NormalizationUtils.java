package com.lzouloumis.nn;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.index.NDIndex;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;

/**
 * Utility class providing normalization features for NDArrays
 */
public class NormalizationUtils {

    private NDManager manager;

    public NormalizationUtils(NDManager manager) {
        this.manager = manager;
    }

    public NDArray preprocessFeatures(NDArray features){
        NDArray rowsSum = features.sum(new int[]{1});
        NDArray inverse = rowsSum.pow(-1);
        inverse = replaceInfinities(inverse);
        return diagonal(inverse).dot(features);
    }

    public NDArray preprocessAdjacencyMatrix(NDArray adjacencyMatrix){
        adjacencyMatrix = adjacencyMatrix.add(identityMatrix(adjacencyMatrix.size(0)));
        return normalizeAdjacencyMatrix(adjacencyMatrix);
    }

    private NDArray normalizeAdjacencyMatrix(NDArray adjacencyMatrix){
        NDArray rowsSum = adjacencyMatrix.sum(new int[]{1});
        NDArray inverseSquareRoot = rowsSum.pow(-0.5);
        inverseSquareRoot = replaceInfinities(inverseSquareRoot);
        NDArray diagonalMatrix = diagonal(inverseSquareRoot);
        return adjacencyMatrix.dot(diagonalMatrix).transpose().dot(diagonalMatrix);
    }


    private NDArray replaceInfinities(NDArray array){
        return array.reshape(new Shape(-1,1,1)).sequenceMask(array.neq(Float.POSITIVE_INFINITY).toType(DataType.FLOAT32, false)).reshape(-1);
    }

    private NDArray diagonal(NDArray array){
        long length = array.size();
        NDArray diagonalArray = manager.zeros(new Shape(length, length), DataType.FLOAT32);
        for (int i = 0; i < diagonalArray.size(0); i++) {
            diagonalArray.set(new NDIndex(i+", "+i),array.getFloat(i));
        }
        return diagonalArray;
    }

    private NDArray identityMatrix(long dimension){
        if(dimension >= Integer.MAX_VALUE)
            throw new RuntimeException("Cannot create identity matrix with dimensions " + dimension + "x" +dimension);
        return manager.eye((int)dimension, (int)dimension, 0, DataType.FLOAT32);
    }

}
