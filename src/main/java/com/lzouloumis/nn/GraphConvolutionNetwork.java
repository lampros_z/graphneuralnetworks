package com.lzouloumis.nn;

import ai.djl.Device;
import ai.djl.MalformedModelException;
import ai.djl.Model;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import ai.djl.nn.Activation;
import ai.djl.nn.Block;
import ai.djl.nn.SequentialBlock;
import ai.djl.nn.norm.Dropout;
import ai.djl.training.DefaultTrainingConfig;
import ai.djl.training.GradientCollector;
import ai.djl.training.Trainer;
import ai.djl.training.initializer.XavierInitializer;
import ai.djl.training.loss.Loss;
import ai.djl.training.optimizer.Optimizer;
import ai.djl.training.tracker.Tracker;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Graph Convolution Network implementation similar to https://arxiv.org/abs/1609.02907
 * This class contains all the necessary information and methods to build and train a graph convolution layer given a graph dataset as input.
 */
public class GraphConvolutionNetwork implements AutoCloseable, ModelPersistable {


    public static final String MODEL_NAME = "GRAPH_CONVOLUTION_NETWORK";
    /*Hyperparameter default values*/
    public static final int NUMBER_OF_LAYERS = 2;
    public static final float DROPOUT_RATE = 0.5F;
    public static final int NUMBER_OF_HIDDEN_UNITS = 16;
    public static final double WEIGHT_DECAY = 5e-4;
    public static final double LEARNING_RATE = 1e-2;
    public static final int EPOCHS = 200;

    /*Hyperparameters*/
    private  float dropoutRate;
    private  int numberOfHiddenUnits;
    private  double weightDecay;
    private  double learningRate;
    private  int epochs;

    /**
     * The manager provided by the djl library(https://djl.ai/) for managing NDArrays
     */
    private final NDManager manager;
    /**
     * Default model provided by djl. This model contains the graph convolution network
     */
    private Model model;
    /**
     * Default sequential block provided by djl containing all necessary child blocks
     */
    private SequentialBlock net;
    /**
     * The adjacency matrix provided as input to the network
     */
    private  NDArray adjacency;
    /**
     * The feature matrix provided as input to the network
     */
    private  NDArray features;
    /**
     * The matrix with node labels provided as input to the model
     */
    private  NDArray labels;
    /**
     * Field containing the number of hidden layers
     */
    private  int numberOfLayers;
    /**
     * A long array containing the dimensions of each layer
     */
    private  long[] layerDimensions;
    /**
     * The loss function of the network(default: Softmaxcrossentropy loss for classification)
     */
    private Loss loss;
    /**
     * The default trainer interface provided by the djl library for providing a session for model training
     */
    private Trainer trainer;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the training phase.
     */
    private  String idxTrain;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the validation phase.
     */
    private  String idxVal;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the testing phase.
     */
    private  String idxTest;
    /**
     * An array for storing the train accuracy value of each epoch
     */
    private double[] trainAccuracy;
    /**
     * An array for storing the train loss value of each epoch
     */
    private double[] trainLoss;
    /**
     * An array for storing the validation accuracy value of each epoch
     */
    private  double[] validationAccuracy;

    /**
     * Default constructor used from the builder class to provide instances of this class
     * @param builder A builder object containing the initialization values
     */
    private GraphConvolutionNetwork(Builder builder){
        GraphDataset graphDataset = builder.graphDataset;
        this.manager = NDManager.newBaseManager(builder.device);
        this.dropoutRate = builder.dropoutRate;
        this.weightDecay = builder.weightDecay;
        this.learningRate = builder.learningRate;
        this.epochs = builder.epochs;
        this.numberOfLayers = builder.numberOfLayers;
        this.numberOfHiddenUnits = builder.numberOfHiddenUnits;
        this.idxTrain = builder.trainRange;
        this.idxVal = builder.validationRange;
        this.idxTest = builder.testRange;
        if(graphDataset != null){
            this.adjacency = graphDataset.getAdjacencyMatrix();
            this.features = graphDataset.getFeatureMatrix();
            this.labels = graphDataset.getLabelMatrix();

            if( builder.layerDimensions == null)
               this.layerDimensions = new long[]{graphDataset.getFeaturesDimension(), numberOfHiddenUnits, numberOfHiddenUnits, graphDataset.getClassesNo()};
            else
                this.layerDimensions = builder.layerDimensions;
            this.idxTrain = graphDataset.getTrainRange();
            this.idxVal = graphDataset.getValidationRange();
            this.idxTest = graphDataset.getTestRange();

            buildModel();
        }
    }

    @Override
    public void saveModel(String filename) {

        try(FileOutputStream fileOutputStream = new FileOutputStream(filename + ARCHITECTURE_FILE_EXTENSION)){
            /*Save model architecture*/
            FileChannel fileChannel = fileOutputStream.getChannel();
            byte[] adjacencyByteArray = adjacency.encode();
            int capacity = 8*layerDimensions.length + 4*3 + 4  + adjacencyByteArray.length;
            ByteBuffer buffer = ByteBuffer.allocate(capacity);
            buffer.putInt(adjacencyByteArray.length);
            buffer.putInt(layerDimensions.length);
            buffer.putInt(numberOfLayers);
            buffer.putFloat(dropoutRate);
            for( long dimension : layerDimensions ){
                buffer.putLong(dimension);
            }
            buffer.put(adjacencyByteArray);
            buffer.rewind();
            fileChannel.write(buffer);
            fileChannel.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        try( DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(filename))){
            /*Save model parameters*/
            for(Block block : net.getChildren().values()){
                if (block instanceof GraphConvolutionLayer){
                    block.saveParameters(dataOutputStream);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadModel(String filename) {

        try( FileInputStream fileInputStream = new FileInputStream(filename + ARCHITECTURE_FILE_EXTENSION)){

            NDArray adjacencyMatrix;
            long[] layerDimensions;
            /*Load model architecture*/
            FileChannel fileChannel = fileInputStream.getChannel();

            ByteBuffer memoryBuffer = ByteBuffer.allocate(8);
            fileChannel.read(memoryBuffer);
            memoryBuffer.rewind();
            int adjacencyMatrixLength = memoryBuffer.getInt();
            int layerDimensionsArrayLength = memoryBuffer.getInt();

            int capacity = layerDimensionsArrayLength*8 + 4*3 + 4 + adjacencyMatrixLength;
            ByteBuffer byteBuffer = ByteBuffer.allocate(capacity);
            fileChannel.read(byteBuffer, 0);
            byteBuffer.rewind();

            adjacencyMatrixLength = byteBuffer.getInt();
            layerDimensionsArrayLength = byteBuffer.getInt();
            int numberOfLayers = byteBuffer.getInt();
            float dropoutRate = byteBuffer.getFloat();

            layerDimensions = new long[layerDimensionsArrayLength];
            for(int i=0; i < layerDimensionsArrayLength; i++){
                layerDimensions[i] = byteBuffer.getLong();
            }
            byte[] adjacencyMatrixBytes = new byte[adjacencyMatrixLength];

            for(int i = 0; i < adjacencyMatrixLength; i++){
                adjacencyMatrixBytes[i] = byteBuffer.get();
            }
            adjacencyMatrix = NDArray.decode(manager, adjacencyMatrixBytes);
            fileChannel.close();
            /* End of loading data */

            /* Reload the model */
            model = Model.newInstance(MODEL_NAME);
            model.setDataType(DataType.FLOAT32);
            net = new SequentialBlock();

            for(int layer = 0; layer < numberOfLayers; ++layer){

                GraphConvolutionLayer graphConvolutionLayer = new GraphConvolutionLayer(layerDimensions[2*layer], layerDimensions[2*layer+1], adjacencyMatrix);
                graphConvolutionLayer.setInitializer(new XavierInitializer());
                Shape initShape =  new Shape(layerDimensions[2*layer], layerDimensions[2*layer+1]);
                graphConvolutionLayer.initialize(manager, DataType.FLOAT32, initShape);
                net.add(graphConvolutionLayer);

                if(layer < numberOfLayers-1) {
                    net.add(Activation::relu);
                    net.add(Dropout.builder().optRate(dropoutRate).build());
                }

            }
            net.add(new LogSoftmaxBlock());
            model.setBlock(net);

            /*Logit softmax cross entropy loss*/
            loss = Loss.softmaxCrossEntropyLoss("SoftmaxCrossEntropyLoss", 1, -1, true, true);
            Tracker lrt = Tracker.fixed((float) learningRate);
            Optimizer adam = Optimizer.adam().optLearningRateTracker(lrt).optWeightDecays((float) weightDecay).build();
            DefaultTrainingConfig config = new DefaultTrainingConfig(loss).optOptimizer(adam).optDevices(new Device[]{manager.getDevice()});
            trainer = model.newTrainer(config);
            /* End of reload*/

        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        /*Reload the model parameters*/

        try( DataInputStream dataInputStream = new DataInputStream(new FileInputStream(filename))){
            /*Load model parameters*/
            for(Block block : net.getChildren().values()){
                if (block instanceof GraphConvolutionLayer){
                    block.loadParameters(manager, dataInputStream);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (MalformedModelException e) {
            System.err.println(e.getMessage());
        }


    }

    /**
     *  Builder class is used to define the internal state of GraphConvolutionNetwork Objects and then
     * initialize them by using the private constructor.
     */
    public static class Builder{

        private GraphDataset graphDataset;
        private Device device;
        private int epochs = EPOCHS;
        private float dropoutRate = DROPOUT_RATE;
        private int numberOfHiddenUnits = NUMBER_OF_HIDDEN_UNITS;
        private double weightDecay = WEIGHT_DECAY;
        private double learningRate = LEARNING_RATE;
        private int numberOfLayers = NUMBER_OF_LAYERS;
        private long[] layerDimensions;
        private String trainRange;
        private String validationRange;
        private String testRange;

        public Builder graphDataset(final GraphDataset graphDataset){
            this.graphDataset = graphDataset;
            return this;
        }

        public Builder optNumberOfLayers(final int layers){
            this.numberOfLayers = layers;
            return this;
        }

        public Builder optLayerDimensions(final long[] layerDimensions){
            this.layerDimensions = layerDimensions;
            return this;
        }

        public Builder optDevice(final Device device){
            this.device = device;
            return this;
        }

        public Builder optEpochs(final int epochs){
            this.epochs = epochs;
            return this;
        }

        public Builder optTrainRange(final String trainRange){
            this.trainRange = trainRange;
            return this;
        }

        public Builder optValidationRange(final String validationRange){
            this.validationRange = validationRange;
            return this;
        }

        public Builder optTestRange(final String testRange){
            this.testRange = testRange;
            return this;
        }

        public Builder optDropoutRate(final float dropoutRate){
            this.dropoutRate = dropoutRate;
            return this;
        }

        public Builder optHiddenUnits(final int numberOfHiddenUnits){
            this.numberOfHiddenUnits = numberOfHiddenUnits;
            return this;
        }

        public Builder optWeightDecay(final double weightDecay ){
            this.weightDecay = weightDecay;
            return this;
        }

        public Builder optLearningRate(final double learningRate ){
            this.learningRate = learningRate;
            return this;
        }

        public GraphConvolutionNetwork build(){
            return new GraphConvolutionNetwork(this);
        }

    }

    /**
     * Method responsible for creating the graph convolution model
     */
    private void buildModel() {

        model = Model.newInstance(MODEL_NAME);
        model.setDataType(DataType.FLOAT32);
        net = new SequentialBlock();

        for(int layer = 0; layer < numberOfLayers; ++layer){

            GraphConvolutionLayer graphConvolutionLayer = new GraphConvolutionLayer(layerDimensions[2*layer], layerDimensions[2*layer+1], adjacency);
            graphConvolutionLayer.setInitializer(new XavierInitializer());
            Shape initShape =  new Shape(layerDimensions[2*layer], layerDimensions[2*layer+1]);
            graphConvolutionLayer.initialize(manager, DataType.FLOAT32, initShape);
            net.add(graphConvolutionLayer);

            if(layer < numberOfLayers-1) {
                net.add(Activation::relu);
                net.add(Dropout.builder().optRate(dropoutRate).build());
            }

        }
        net.add(new LogSoftmaxBlock());
        model.setBlock(net);

        /*Logit softmax cross entropy loss*/
        loss = Loss.softmaxCrossEntropyLoss("SoftmaxCrossEntropyLoss", 1, -1, true, true);
        Tracker lrt = Tracker.fixed((float) learningRate);
        Optimizer adam = Optimizer.adam().optLearningRateTracker(lrt).optWeightDecays((float) weightDecay).build();
        DefaultTrainingConfig config = new DefaultTrainingConfig(loss).optOptimizer(adam).optDevices(new Device[]{manager.getDevice()});
        trainer = model.newTrainer(config);
    }

    /**
     * Train method is used at the train phase of the model
     */
    public void train() {
        trainAccuracy = new double[epochs];
        trainLoss = new double[epochs];
        validationAccuracy = new double[epochs];

        double accuracyVal;
        NDArray tempX;
        NDArray tempy;
        NDList yHat;
        NDArray lossValue;

        for (int epoch = 1; epoch <= epochs; epoch++) {
            System.out.println("Running epoch " + epoch + "...... ");
            try(NDManager localManager = trainer.getManager().newSubManager(trainer.getManager().getDevice())) {
                tempX = localManager.create(features.getShape());
                tempy = localManager.create(labels.getShape());
                features.copyTo(tempX);
                labels.copyTo(tempy);
                try (GradientCollector gradientCollector = trainer.newGradientCollector()) {

                    yHat = trainer.forward(new NDList(tempX)); // net function call
                    lossValue = loss.evaluate(new NDList(tempy.get(idxTrain)), new NDList(yHat.head().get(idxTrain)));
                    accuracyVal = accuracy(new NDList(tempy.get(idxTrain)), new NDList(yHat.head().get(idxTrain)));
                    trainAccuracy[epoch - 1] = accuracyVal;
                    gradientCollector.backward(lossValue);
                    trainLoss[epoch - 1] = lossValue.reshape(new Shape(1)).getFloat(0);
                    System.out.println("\nloss: " + lossValue.reshape(new Shape(1)).getFloat(0) + ", acc: " + accuracyVal);
                    trainer.step();

                    yHat.close();
                    lossValue.close();
                }

                //Validation
                validation(idxVal, new NDList(tempX, tempy), epoch);

            }

        }
    }

    /**
     * Test method is used at the testing phase of the model
     */
    public void test() {
        NDList yHat = trainer.evaluate(new NDList(features)); // predict class
        double valAcc = accuracy(new NDList(labels.get(idxTest)), new NDList(yHat.head().get(idxTest)));
        System.out.println("Test Validation accuracy: " + valAcc);
    }

    /**
     * Method used at the validation phase of the model to calculate the validation accuracy at the specified epoch
     * @param range A string formatted as "startIndex:endIndex" which fetches specific nodes from the whole graph dataset to be tested during the validation phase
     * @param params An NDList containing at the first index the features and at the second index the labels of the initial dataset
     * @param epoch The current training epoch
     */
    public void validation(String range, NDList params, int epoch) {
        NDList yHat = trainer.evaluate(new NDList(params.get(0))); // predict class
        double valAcc = accuracy(new NDList(params.get(1).get(range)), new NDList(yHat.get(0).get(range)));
        validationAccuracy[epoch-1] = valAcc;
        System.out.println("Validation accuracy: " + valAcc);
    }

    /**
     * Closes all closable resources
     */
    @Override
    public void close(){
        trainer.close();
        features.close();
        labels.close();
    }


    public Model getModel() {
        return model;
    }

    public double[] getTrainAccuracy() {
        return trainAccuracy;
    }

    public double[] getTrainLoss() {
        return trainLoss;
    }

    public double[] getValidationAccuracy() {
        return validationAccuracy;
    }

    private NDList decodeOneHot(NDList labels){
        NDArray label = labels.head();
        NDArray decodedArray = label.argMax(1).toType(DataType.FLOAT32, false);
        return new NDList(decodedArray);
    }

    /**
     * Method that given the label matrix and the prediction matrix of the model calculates the accuracy
     * @param labels The label matrix
     * @param predictions The matrix with the predictions
     * @return The accuracy of the predictions
     */
    private double accuracy(NDList labels, NDList predictions){
        NDArray label = labels.head();
        NDArray prediction = predictions.head();
        checkShapes(prediction, label);
        /* Multi-class, sparse label */
        NDArray predictionReduced = prediction.argMax(1);
        long total = label.size();
        try (NDArray nd = label.toType(DataType.INT64, true)) {
            NDArray correct = predictionReduced.toType(DataType.INT64, false).eq(nd).countNonzero().reshape(new Shape(1));
            return  (double)correct.getLong(0)/total;
        }
    }

    /**
     * Checks if the number of nodes of the predictions matrix and the labels matrix is equal
     * @param predictions The predictions matrix
     * @param labels The labels matrix
     */
    private void checkShapes(NDArray predictions, NDArray labels){
        if (labels.getShape().get(0) != predictions.getShape().get(0)) {
            throw new IllegalArgumentException(
                    "The size of labels("
                            + labels.size()
                            + ") does not match that of predictions("
                            + predictions.size()
                            + ")");
        }
    }

}
