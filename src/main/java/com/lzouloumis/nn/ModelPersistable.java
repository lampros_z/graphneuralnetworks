package com.lzouloumis.nn;

/**
 * Model persistable interface defines functionality for persisting graph neural networks' model parameters
 */
public interface ModelPersistable {
    /**
     * Each model should save its architecture saved to a file with an extension specified by this constant
     */
    String ARCHITECTURE_FILE_EXTENSION = ".architecture";

    /**
     * Method responsible for saving the model's state to the file specified
     * @param filename The name of the file where the model will be persisted
     */
    void saveModel(String filename);

    /**
     * Method repsonsible for loading the model's state from the file specified
     * @param filename The name of the file from where the model will be loaded
     */
    void loadModel(String filename);
}
