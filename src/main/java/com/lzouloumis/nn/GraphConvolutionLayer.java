package com.lzouloumis.nn;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.Shape;
import ai.djl.nn.AbstractBlock;
import ai.djl.nn.Parameter;
import ai.djl.nn.ParameterType;
import ai.djl.training.ParameterStore;
import ai.djl.util.PairList;

/**
 * Simple graph convolution layer, similar to https://arxiv.org/abs/1609.02907
 */
public class GraphConvolutionLayer extends AbstractBlock {

    private static final byte VERSION = 2;

    /**
     * The weight matrix of this layer
     */
    private final Parameter weight;
    /**
     * The bias added to the model function
     */
    private final Parameter bias;

    /**
     * Input features
     */
    private final long  inputSize;
    /**
     * Output features
     */
    private final long outputSize;
    private final NDArray adjacency;
    /**
     * Constructs a graph convolution layer that can be added to a sequential block.
     * @param inputSize The input features size
     * @param outputSize The output features size
     */
    public GraphConvolutionLayer(long inputSize, long outputSize, NDArray adjacency) {
        super(VERSION);
        this.inputSize = inputSize;
        this.outputSize = outputSize;
        this.adjacency = adjacency;
        weight = addParameter(new Parameter("weight", this, ParameterType.WEIGHT), new Shape(inputSize, outputSize));
        bias = addParameter(new Parameter("bias", this, ParameterType.BIAS), new Shape(outputSize));
    }

    /**
     * Method responsible for implementing forward propagation under the hood.
     * @param parameterStore A map containing mirrors of a parameter on other devices
     * @param inputs A list containing either the features matrix only or both the features and label matrices (In this forward propagation method only the features matrix is needed).
     * @param training If training the model or not
     * @param params Additional model parameters
     * @return the result of the calculation of all the intermediate variables
     */
    @Override
    public NDList forwardInternal(ParameterStore parameterStore, NDList inputs, boolean training, PairList<String, Object> params) {
        NDArray features = inputs.singletonOrThrow();
        NDArray w1 = weight.getArray();
        NDArray b = bias.getArray();
        NDArray support = features.matMul(w1);
        NDArray result = adjacency.matMul(support);
        result = result.add(b);
        return new NDList(result);

    }

    /**
     * Returns the shape of this layer
     * @param manager Manager used to create NDArrays
     * @param inputShapes An array of shapes given by the user
     * @return shape of this gcn layer
     */
    @Override
    public Shape[] getOutputShapes(NDManager manager, Shape[] inputShapes) {
        return new Shape[]{new Shape(inputSize, outputSize)};
    }

}
