package com.lzouloumis.nn;

import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import ai.djl.nn.*;
import ai.djl.nn.core.Linear;
import ai.djl.nn.norm.Dropout;
import ai.djl.training.ParameterStore;
import ai.djl.util.PairList;

/**
 * Simple graph attention layer similar to https://arxiv.org/abs/1710.10903
 */
public class GraphAttentionLayer extends AbstractBlock {
    /*Constants defining default names for blocks*/
    public static final String SKIP_CONNECTION_BLOCK_NAME = "skipConnection";
    public static final String BIAS_PARAMETER_NAME = "bias";
    public static final String SCORING_SOURCE_PARAM_NAME = "scoringSource";
    public static final String SCORING_TARGET_PARAM_NAME = "scoringTarget";
    public static final String LINEAR_PROJECTION_BLOCK_NAME = "LinearProjectionBlock";

    private static final byte VERSION = 4;
    /**
     * NDArray containing the connectivity mask used at the softmax calculation
     */
    private final NDArray connectivityMask;
    /**
     * Field containing the number of attention heads
     */
    private final int numberOfAttentionHeads;
    /**
     * Features size over the first-axis
     */
    private final int featuresOutDimension;
    /**
     * Block containing an activation function
     */
    private final Block activation;
    /**
     * The bias added to the model function
     */
    private final boolean bias;
    /**
     * Whether to concatenate the attention weights or average them
     */
    private final boolean concat;
    /**
     * Whether to apply skip connection or not
     */
    private final boolean skipConnection;
    /**
     * Skip connection block to prevent over-smoothing
     */
    private final Block skipConnectionBlock;
    /**
     * Default linear block
     */
    private final Block linearProjection;
    /**
     * Scoring function parameter
     */
    private final Parameter scoringFunctionSource;
    /**
     * Scoring function parameter target
     */
    private final Parameter scoringFunctionTarget;
    /**
     * Bias parameter
     */
    private final Parameter biasParameter;
    /**
     * Dropout probability
     */
    private final float dropoutProbability;

    /**
     * private constructor used to create graph attention blocks.
     * @param builder Builder class instance that contains all the necessary information to call this private constructor
     */
    private GraphAttentionLayer(Builder builder) {
        super(VERSION);
        this.connectivityMask = builder.connectivityMask;
        this.numberOfAttentionHeads = builder.numberOfAttentionHeads;
        this.featuresOutDimension = builder.featuresOutDimension;
        this.activation = builder.activation;
        this.dropoutProbability = builder.dropoutProbability;
        this.bias = builder.bias;
        this.concat = builder.concat;
        this.skipConnection = builder.skipConnection;

        linearProjection = addChildBlock(LINEAR_PROJECTION_BLOCK_NAME, Linear.builder().setUnits(numberOfAttentionHeads*featuresOutDimension).optBias(false).build());
        scoringFunctionSource = addParameter(new Parameter(SCORING_SOURCE_PARAM_NAME, this, ParameterType.WEIGHT), new Shape(1, numberOfAttentionHeads, featuresOutDimension));
        scoringFunctionTarget = addParameter(new Parameter(SCORING_TARGET_PARAM_NAME, this, ParameterType.WEIGHT), new Shape(1, numberOfAttentionHeads, featuresOutDimension));

        if (bias && concat)
            biasParameter = addParameter(new Parameter(BIAS_PARAMETER_NAME, this, ParameterType.BIAS), new Shape(numberOfAttentionHeads * featuresOutDimension));
        else if (bias && !concat)
            biasParameter = addParameter(new Parameter(BIAS_PARAMETER_NAME, this, ParameterType.BIAS), new Shape(featuresOutDimension));

        else biasParameter = null;

        if(skipConnection) skipConnectionBlock = addChildBlock(SKIP_CONNECTION_BLOCK_NAME, Linear.builder().setUnits(numberOfAttentionHeads*featuresOutDimension).optBias(false).build());
        else skipConnectionBlock = null;

    }

    /**
     * Method responsible for implementing forward propagation under the hood.
     * @param parameterStore A map containing mirrors of a parameter on other devices
     * @param inputs A list containing either the features matrix only or both the features and label matrices (In this forward propagation method only the features matrix is needed).
     * @param training If training the model or not
     * @param params Additional model parameters
     * @return the result of the calculation of all the intermediate variables
     */
    @Override
    public NDList forwardInternal(ParameterStore parameterStore, NDList inputs, boolean training, PairList<String, Object> params) {

        NDArray features = inputs.get(0);
        if(training)
            features = Dropout.dropout(features, dropoutProbability).head();
        ParameterStore parameterStore1 = new ParameterStore(NDManager.newBaseManager(features.getDevice()), false);
        NDArray nodesFeaturesProjection = linearProjection.forward(parameterStore1, new NDList(features), training).singletonOrThrow().reshape(-1, numberOfAttentionHeads, featuresOutDimension);
        if(training)
            nodesFeaturesProjection = Dropout.dropout(nodesFeaturesProjection).head();

        NDArray inScoreSrc = scoringFunctionSource.getArray();
        NDArray inScoreTrg = scoringFunctionTarget.getArray();

        NDArray sourceScores = nodesFeaturesProjection.mul(inScoreSrc).sum(new int[]{-1}, true);

        NDArray targetScores = nodesFeaturesProjection.mul(inScoreTrg).sum(new int[]{-1}, true);
        sourceScores = sourceScores.transpose(1, 0, 2);
        targetScores = targetScores.transpose(1, 2, 0);

        NDArray allScores = Activation.leakyRelu(sourceScores.add( targetScores ), 0.2F);

        NDArray allAttentionCoefficients = allScores.add(connectivityMask).softmax(-1);
        NDArray outNodesFeatures  = allAttentionCoefficients.matMul(nodesFeaturesProjection.transpose(1, 0, 2));
        outNodesFeatures = outNodesFeatures.transpose(1, 0, 2);

        if (skipConnection) {
            if(outNodesFeatures.size(2) == features.size(1)) {
                outNodesFeatures = outNodesFeatures.add(features.expandDims(1));
            }else {
                outNodesFeatures = outNodesFeatures.add(skipConnectionBlock.forward(parameterStore1, new NDList(features), false).singletonOrThrow().reshape(-1, numberOfAttentionHeads, featuresOutDimension));
            }
        }
        if (concat) {
            outNodesFeatures = outNodesFeatures.reshape(new Shape(-1, numberOfAttentionHeads*featuresOutDimension));
        }else {
            outNodesFeatures = outNodesFeatures.mean(new int[]{1});
        }
        if(bias){
            outNodesFeatures = outNodesFeatures.add(biasParameter.getArray());
        }
        if(activation != null)
            outNodesFeatures = activation.forward(parameterStore1, new NDList(outNodesFeatures), training).singletonOrThrow();
        return new NDList(outNodesFeatures);
    }

    /**
     * Method to initialize all child blocks
     * @param manager Manager used to create NDArrays
     * @param dataType Enumeration containing the default data type provided by djl
     * @param inputShapes An array containing the necessary shapes to initialize the child blocks
     */
    @Override
    protected void initializeChildBlocks(NDManager manager, DataType dataType, Shape... inputShapes) {

        if(linearProjection == null)
            return;
        linearProjection.initialize(manager, dataType, inputShapes[0]);

        if(skipConnectionBlock == null)
            return;
        skipConnectionBlock.initialize(manager, dataType, inputShapes[0]);

    }

    /**
     * Returns the shape of this layer
     * @param manager Manager used to create NDArrays
     * @param inputShapes An array of shapes given by the user
     * @return shape of this gcn layer
     */
    @Override
    public Shape[] getOutputShapes(NDManager manager, Shape[] inputShapes) {
        return new Shape[0];
    }

    /**
     * Builder class creates instances of this class by calling the private constructor
     */
    public static class Builder{
        private NDArray connectivityMask;
        private int numberOfAttentionHeads;
        private int featuresOutDimension;
        private Block activation;
        private float dropoutProbability = 0.6F;
        private boolean bias = true;
        private boolean concat = true;
        private boolean skipConnection = true;
        public Builder connectivityMask(final NDArray connectivityMask){
            this.connectivityMask = connectivityMask;
            return this;
        }

        public Builder numberOfAttentionHeads(final int numberOfAttentionHeads){
            this.numberOfAttentionHeads = numberOfAttentionHeads;
            return this;
        }

        public Builder featuresOutDimension(final int featuresOutDimension){
            this.featuresOutDimension = featuresOutDimension;
            return this;
        }

        public Builder activation(final Block activation){
            this.activation = activation;
            return this;
        }

        public Builder optDropoutProbability(final float dropoutProbability){
            this.dropoutProbability = dropoutProbability;
            return this;
        }

        public Builder optBias(final boolean bias){
            this.bias = bias;
            return this;
        }

        public Builder optConcat(final boolean concat){
            this.concat = concat;
            return this;
        }

        public Builder optSkipConnection(final boolean skipConnection){
            this.skipConnection = skipConnection;
            return this;
        }

        public GraphAttentionLayer build(){
            return new GraphAttentionLayer(this);
        }

    }

}
