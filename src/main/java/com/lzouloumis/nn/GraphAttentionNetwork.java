package com.lzouloumis.nn;

import ai.djl.Device;
import ai.djl.MalformedModelException;
import ai.djl.Model;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import ai.djl.nn.Activation;
import ai.djl.nn.Block;
import ai.djl.nn.SequentialBlock;
import ai.djl.training.DefaultTrainingConfig;
import ai.djl.training.GradientCollector;
import ai.djl.training.Trainer;
import ai.djl.training.initializer.XavierInitializer;
import ai.djl.training.loss.Loss;
import ai.djl.training.optimizer.Optimizer;
import ai.djl.training.tracker.Tracker;
import org.apache.commons.lang3.ArrayUtils;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Graph Attention Network implementation similar to https://arxiv.org/abs/1710.10903
 * This class contains all the necessary information and methods to build and train a graph attention layer given a graph dataset as input.
 */
public class GraphAttentionNetwork implements ModelPersistable {
    public static final String MODEL_NAME = "Graph Attention Network";
    /*Hyperparameters*/
    private static final int NUMBER_OF_EPOCHS = 200;
    private static final double LEARNING_RATE = 0.01;
    private static final double WEIGHT_DECAY = 5e-4;
    private static final float DROPOUT_RATE = 0.5F;

    /**
     * Default model provided by djl. This model contains the graph attention network
     */
    private Model model;
    /**
     * Default sequential block provided by djl containing all necessary child blocks
     */
    private SequentialBlock network;
    /**
     * The default trainer interface provided by the djl library for providing a session for model training
     */
    private Trainer trainer;
    /**
     * The manager provided by the djl library(https://djl.ai/) for managing NDArrays
     */
    private final NDManager manager;
    /**
     * Field containing the number of epochs
     */
    private final int epochs;
    /**
     * Field containing the dropout probability
     */
    private final float dropoutRate;
    /**
     * Field containing the network's optimizer
     */
    private final Optimizer optimizer;
    /**
     * The loss function of the network(default: Softmaxcrossentropy loss for classification)
     */
    private final Loss lossFunction;
    /**
     * The graph dataset given as input to the network
     */
    private final GraphDataset graphDataset;
    /**
     * Field containing the number of hidden layers
     */
    private final int numberOfLayers;
    /**
     * Array containing the number of attention heads for each layer
     */
    private final int[] numberOfAttentionHeadsPerLayer;
    /**
     * Array containing the features dimensions in each layer
     */
    private final int[] numberOfFeaturesPerLayer;
    /**
     * Whether to add skip connection block or not(Skip connections are used to prevent over-smoothing)
     */
    private final boolean addSkipConnection;
    /**
     * Whether to add bias to the network or not
     */
    private final boolean addBias;
    /**
     * Connectivity mask is used to calculate the attention coefficients effectively
     */
    private  NDArray connectivityMask;
    /**
     * The feature matrix provided to this network
     */
    private  NDArray featureMatrix;
    /**
     * The matrix of labels for each node provided to this network
     */
    private  NDArray nodeLabels;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the training phase.
     */
    private  String trainRange;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the validation phase.
     */
    private  String validationRange;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the testing phase.
     */
    private  String testRange;
    /**
     * An array for storing the train accuracy value of each epoch
     */
    private double[] trainAccuracy;
    /**
     * An array for storing the validation accuracy value of each epoch
     */
    private double[] validationAccuracy;
    /**
     * An array for storing the train loss value of each epoch
     */
    private double[] trainLoss;

    /**
     * Default constructor used from the builder class to provide instances of this class
     * @param builder A builder object containing the initialization values
     */
    private GraphAttentionNetwork(Builder builder) {
        manager = NDManager.newBaseManager(builder.device);
        epochs = builder.epochs;
        dropoutRate = builder.dropoutRate;
        optimizer = builder.optimizer;
        lossFunction = builder.lossFunction;
        graphDataset = builder.graphDataset;
        if(graphDataset != null) {
            connectivityMask = buildConnectivityMask(graphDataset.getAdjacencyMatrix());
            featureMatrix = graphDataset.getFeatureMatrix();
            nodeLabels = graphDataset.getLabelMatrix();
            trainRange = graphDataset.getTrainRange();
            validationRange = graphDataset.getValidationRange();
            testRange = graphDataset.getTestRange();
        }
        numberOfLayers = builder.numberOfLayers;
        numberOfAttentionHeadsPerLayer = builder.numberOfAttentionHeadsPerLayer;
        numberOfFeaturesPerLayer = builder.numberOfFeaturesPerLayer;
        addSkipConnection = builder.addSkipConnection;
        addBias = builder.addBias;
        if(graphDataset != null)
            buildModel();
    }

    /**
     * Replaces all zeros with negative infinity and all ones with zeros.
     * @param adjacencyMatrix The adjacency matrix of the graph
     * @return The connectivity mask matrix
     */
    private NDArray buildConnectivityMask(NDArray adjacencyMatrix) {
        NDArray flattenedAdjacency = adjacencyMatrix.flatten();
        NDArray support = flattenedAdjacency.reshape(new Shape(-1,1,1)).sequenceMask(flattenedAdjacency.neq(0).toType(DataType.FLOAT32, false), Float.NEGATIVE_INFINITY).squeeze();
        return  support.reshape(new Shape(-1,1,1)).sequenceMask(support.neq(1).toType(DataType.FLOAT32, false), 0).reshape(adjacencyMatrix.getShape());
    }

    /**
     * Method responsible for creating the graph attention model
     */
    private void buildModel(){
        if(numberOfAttentionHeadsPerLayer.length != numberOfFeaturesPerLayer.length - 1)
            throw new IllegalArgumentException("Invalid parameters given: Attention heads, Features per layer ");
        int[] firstAttentionHead = new int[]{1};
        int[] attentionHeadsPerLayer = ArrayUtils.addAll(firstAttentionHead, numberOfAttentionHeadsPerLayer);

        model = Model.newInstance(MODEL_NAME);
        model.setDataType(DataType.FLOAT32);

        network = new SequentialBlock();
        for (int layer = 0; layer < numberOfLayers; layer++) {
            GraphAttentionLayer graphAttentionLayer = new GraphAttentionLayer
                    .Builder()
                    .connectivityMask(connectivityMask)
                    .featuresOutDimension(numberOfFeaturesPerLayer[layer+1])
                    .numberOfAttentionHeads(attentionHeadsPerLayer[layer+1])
                    .optConcat( layer < numberOfLayers-1 ? true : false )
                    .activation( layer < numberOfLayers-1 ? Activation.eluBlock(1F) : null )
                    .optDropoutProbability(dropoutRate)
                    .optSkipConnection(addSkipConnection)
                    .optBias(addBias)
                    .build();
            graphAttentionLayer.setInitializer(new XavierInitializer());
            graphAttentionLayer.initialize(manager, DataType.FLOAT32, new Shape[]{new Shape(attentionHeadsPerLayer[layer+1]*numberOfFeaturesPerLayer[layer+1], numberOfFeaturesPerLayer[layer]*attentionHeadsPerLayer[layer])});
            network.add(graphAttentionLayer);
        }

        model.setBlock(network);
        DefaultTrainingConfig config = new DefaultTrainingConfig(lossFunction).optOptimizer(optimizer).optDevices(new Device[]{manager.getDevice()});
        trainer = model.newTrainer(config);

    }

    /**
     * Train method is used at the train phase of the model
     */
    public void train() {
        trainAccuracy = new double[epochs];
        trainLoss = new double[epochs];
        validationAccuracy = new double[epochs];

        double accuracyVal;
        NDArray tempX;
        NDArray tempy;
        for (int epoch = 1; epoch <= epochs; epoch++) {
            System.out.print("Running epoch " + epoch + "...... ");
            try(NDManager localManger = trainer.getManager().newSubManager(manager.getDevice())) {
                tempX = localManger.create(featureMatrix.getShape());
                tempy = localManger.create(nodeLabels.getShape());
                featureMatrix.copyTo(tempX);
                nodeLabels.copyTo(tempy);

                try (GradientCollector gradientCollector = trainer.newGradientCollector()) {
                    NDList yHat = trainer.forward(new NDList(tempX));

                    if (trainRange == null)
                        throw new IllegalArgumentException("Please specify train range");
                    NDArray lossValue = lossFunction.evaluate(new NDList(tempy.get(trainRange)), new NDList(yHat.head().get(trainRange)));

                    accuracyVal = accuracy(new NDList(tempy.get(trainRange)), new NDList(yHat.head().get(trainRange)));
                    trainAccuracy[epoch - 1] = accuracyVal;
                    System.out.println(accuracyVal);
                    gradientCollector.backward(lossValue);
                    trainLoss[epoch - 1] = lossValue.reshape(new Shape(1)).getFloat(0);
                    trainer.step();

                    yHat.close();
                    lossValue.close();
                }
                validation(validationRange, new NDList(tempX, tempy), epoch);
            }

        }

        trainer.close();


    }

    /**
     * Method used at the validation phase of the model to calculate the validation accuracy at the specified epoch
     * @param range A string formatted as "startIndex:endIndex" which fetches specific nodes from the whole graph dataset to be tested during the validation phase
     * @param params An NDList containing at the first index the features and at the second index the labels of the initial dataset
     * @param epoch The current training epoch
     */
    public void validation(String range, NDList params, int epoch) {
        NDList yHat = trainer.evaluate(new NDList(params.get(0))); // predict class
        double valAcc = accuracy(new NDList(params.get(1).get(range)), new NDList(yHat.get(0).get(range)));
        validationAccuracy[epoch-1] = valAcc;
        System.out.println("Validation accuracy: " + valAcc);
    }

    /**
     * Test method is used at the testing phase of the model
     */
    public void test() {
        if(featureMatrix == null || nodeLabels == null || testRange == null)
            return;
        NDList yHat = trainer.evaluate(new NDList(featureMatrix)); // predict class
        double valAcc = accuracy(new NDList(nodeLabels.get(testRange)), new NDList(yHat.head().get(testRange)));
        System.out.println("Test Validation accuracy: " + valAcc);
    }

    /**
     * Method that given the label matrix and the prediction matrix of the model calculates the accuracy
     * @param labels The label matrix
     * @param predictions The matrix with the predictions
     * @return The accuracy of the predictions
     */
    private double accuracy(NDList labels, NDList predictions){
        NDArray label = labels.head();
        NDArray prediction = predictions.head();
        checkShapes(prediction, label);
        /* Multi-class, sparse label */
        NDArray predictionReduced = prediction.argMax(1);
        long total = label.size();
        try (NDArray nd = label.toType(DataType.INT64, false)) {
            NDArray correct = predictionReduced.toType(DataType.INT64, false).eq(nd).countNonzero().reshape(new Shape(1));
            return  (double)correct.getLong(0)/total;
        }
    }

    /**
     * Checks if the number of nodes of the predictions matrix and the labels matrix is equal
     * @param predictions The predictions matrix
     * @param labels The labels matrix
     */
    private void checkShapes(NDArray predictions, NDArray labels){
        if (labels.getShape().get(0) != predictions.getShape().get(0)) {
            throw new IllegalArgumentException(
                    "The size of labels("
                            + labels.size()
                            + ") does not match that of predictions("
                            + predictions.size()
                            + ")");
        }
    }

    @Override
    public void saveModel(String filename) {
        try(FileOutputStream fileOutputStream = new FileOutputStream(filename + ARCHITECTURE_FILE_EXTENSION)){
            /*Save model architecture details*/
            FileChannel fileChannel = fileOutputStream.getChannel();
            byte[] connectivityMaskBytes = connectivityMask.encode();
            int[] firstAttentionHead = new int[]{1};
            int[] attentionHeadsPerLayer = ArrayUtils.addAll(firstAttentionHead, numberOfAttentionHeadsPerLayer);

            int capacity = connectivityMaskBytes.length + 4*numberOfFeaturesPerLayer.length + attentionHeadsPerLayer.length * 4  + 4*5 + 4 + 2*4;
            ByteBuffer buffer = ByteBuffer.allocate(capacity);

            buffer.putInt(capacity);
            buffer.putInt(connectivityMaskBytes.length);
            buffer.putInt(numberOfFeaturesPerLayer.length);
            buffer.putInt(attentionHeadsPerLayer.length);
            buffer.putInt(numberOfLayers);
            buffer.putFloat(dropoutRate);
            buffer.putInt(addSkipConnection ? 1 : 0);
            buffer.putInt(addBias ? 1 : 0);

            for( int number : numberOfFeaturesPerLayer ){
                buffer.putInt(number);
            }

            for(int number : attentionHeadsPerLayer){
                buffer.putInt(number);
            }

            buffer.put(connectivityMaskBytes);
            buffer.rewind();

            fileChannel.write(buffer);
            fileChannel.close();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        try( DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(filename))){
            /*Save model parameters*/
            for(Block block : network.getChildren().values()){
                if (block instanceof GraphAttentionLayer){
                    block.saveParameters(dataOutputStream);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void loadModel(String filename) {
        try( FileInputStream fileInputStream = new FileInputStream(filename + ARCHITECTURE_FILE_EXTENSION)){
            /*Load model architecture*/
            FileChannel fileChannel = fileInputStream.getChannel();
            ByteBuffer memoryBuffer = ByteBuffer.allocate(4);
            fileChannel.read(memoryBuffer);
            memoryBuffer.rewind();
            int capacity = memoryBuffer.getInt();
            memoryBuffer = ByteBuffer.allocate(capacity);
            fileChannel.read(memoryBuffer, 0);
            memoryBuffer.rewind();

            capacity = memoryBuffer.getInt();
            int connectivityMaskBytesLength = memoryBuffer.getInt();
            int numberOfFeaturesArrayLength = memoryBuffer.getInt();
            int attentionHeadsArrayLength  = memoryBuffer.getInt();
            int numberOfLayers = memoryBuffer.getInt();
            float dropoutRate = memoryBuffer.getFloat();
            boolean skipConnection = memoryBuffer.getInt() == 1 ? true : false;
            boolean bias = memoryBuffer.getInt() == 1 ? true : false;

            int[] numberOfFeaturesPerLayer = new int[numberOfFeaturesArrayLength];
            for (int i = 0; i < numberOfFeaturesArrayLength; i++) {
                    numberOfFeaturesPerLayer[i] = memoryBuffer.getInt();
            }

            int[] attentionHeadsPerLayer = new int[attentionHeadsArrayLength];
            for (int i = 0; i < attentionHeadsArrayLength; i++) {
                attentionHeadsPerLayer[i] = memoryBuffer.getInt();
            }

            byte[] connectivityMaskBytes = new byte[connectivityMaskBytesLength];
            for (int i = 0; i < connectivityMaskBytesLength; i++) {
                connectivityMaskBytes[i] = memoryBuffer.get();
            }
            NDArray connectivityMask = NDArray.decode(manager, connectivityMaskBytes);

            fileChannel.close();
            /* End of reloading architecture file*/

            /*Reload model*/
            model = Model.newInstance(MODEL_NAME);
            model.setDataType(DataType.FLOAT32);

            network = new SequentialBlock();
            for (int layer = 0; layer < numberOfLayers; layer++) {
                GraphAttentionLayer graphAttentionLayer = new GraphAttentionLayer
                        .Builder()
                        .connectivityMask(connectivityMask)
                        .featuresOutDimension(numberOfFeaturesPerLayer[layer+1])
                        .numberOfAttentionHeads(attentionHeadsPerLayer[layer+1])
                        .optConcat( layer < numberOfLayers-1 ? true : false )
                        .activation( layer < numberOfLayers-1 ? Activation.eluBlock(1F) : null )
                        .optDropoutProbability(dropoutRate)
                        .optSkipConnection(skipConnection)
                        .optBias(bias)
                        .build();
                graphAttentionLayer.setInitializer(new XavierInitializer());
                graphAttentionLayer.initialize(manager, DataType.FLOAT32, new Shape[]{new Shape(attentionHeadsPerLayer[layer+1]*numberOfFeaturesPerLayer[layer+1], numberOfFeaturesPerLayer[layer]*attentionHeadsPerLayer[layer])});
                network.add(graphAttentionLayer);
            }

            model.setBlock(network);

        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }

        /*Reload the model parameters*/
        try( DataInputStream dataInputStream = new DataInputStream(new FileInputStream(filename))){
            /*Load model parameters*/
            for(Block block : network.getChildren().values()){
                if (block instanceof GraphAttentionLayer){
                    block.loadParameters(manager, dataInputStream);
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (MalformedModelException e) {
            System.err.println(e.getMessage());
        }

    }

    /**
     *  Builder class is used to define the internal state of GraphAttentionNetwork Objects and then
     * initialize them by using the private constructor.
     */
    public static class Builder{
        private Device device = Device.cpu();
        private int epochs = NUMBER_OF_EPOCHS;
        private double learningRate = LEARNING_RATE;
        private double weightDecay = WEIGHT_DECAY;
        private float dropoutRate = DROPOUT_RATE;
        private Tracker tracker = Tracker.fixed((float)learningRate);
        private Optimizer optimizer = Optimizer.adam().optLearningRateTracker(tracker).optWeightDecays((float) weightDecay).build();
        private Loss lossFunction = Loss.softmaxCrossEntropyLoss();
        private GraphDataset graphDataset;
        private String trainRange;
        private String validationRange;
        private String testRange;
        private int numberOfLayers;
        private int[] numberOfAttentionHeadsPerLayer;
        private int[] numberOfFeaturesPerLayer;
        private boolean addSkipConnection = true;
        private boolean addBias = false;

        public Builder dataset(final GraphDataset graphDataset){
            this.graphDataset = graphDataset;
            return this;
        }

        public Builder trainRange(final String trainRange){
            this.trainRange = trainRange;
            return this;
        }

        public Builder validationRange(final String validationRange){
            this.validationRange = validationRange;
            return this;
        }

        public Builder testRange(final String testRange){
            this.testRange = testRange;
            return this;
        }

        public Builder numberOfLayers(final int numberOfLayers){
            this.numberOfLayers = numberOfLayers;
            return this;
        }

        public Builder numberOfAttentionHeadsPerLayer(final int[] numberOfAttentionHeadsPerLayer){
            this.numberOfAttentionHeadsPerLayer = numberOfAttentionHeadsPerLayer;
            return this;
        }

        public Builder numberOfFeaturesPerLayer(final int[] numberOfFeaturesPerLayer){
            this.numberOfFeaturesPerLayer = numberOfFeaturesPerLayer;
            return this;
        }

        public Builder optTracker(final Tracker tracker){
            this.tracker = tracker;
            return this;
        }

        public Builder optOptimizer(final Optimizer optimizer){
            this.optimizer = optimizer;
            return this;
        }

        public Builder optLossFunction(final Loss lossFunction){
            this.lossFunction = lossFunction;
            return this;
        }

        public Builder optDevice(final Device device){
            this.device = device;
            return this;
        }

        public Builder optEpochs(final int epochs){
            this.epochs = epochs;
            return this;
        }

        public Builder optLearningRate(final double learningRate){
            this.learningRate = learningRate;
            return this;
        }

        public Builder optWeightDecay(final double weightDecay){
            this.weightDecay = weightDecay;
            return this;
        }

        public Builder optDropoutRate(final float dropoutRate){
            this.dropoutRate = dropoutRate;
            return this;
        }

        public Builder optAddSkipConnection(final boolean addSkipConnection){
            this.addSkipConnection = addSkipConnection;
            return this;
        }

        public Builder optAddBias(final boolean addBias){
            this.addBias = addBias;
            return this;
        }

        public GraphAttentionNetwork build(){
            return new GraphAttentionNetwork(this);
        }

    }

    public Model getModel() {
        return model;
    }

    public double[] getTrainAccuracy() {
        return trainAccuracy;
    }

    public double[] getValidationAccuracy() {
        return validationAccuracy;
    }

    public double[] getTrainLoss() {
        return trainLoss;
    }
}
