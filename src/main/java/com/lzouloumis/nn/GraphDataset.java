package com.lzouloumis.nn;

import ai.djl.Device;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;

/**
 * GraphDataset class wraps all the necessary information needed for a graph neural network
 */
public class GraphDataset {
    /**
     * The device that will be used by the NDManager
     */
    private final Device device;
    /**
     * The default manager provided by the djl library for manipulating NDArrays
     */
    private final NDManager ndManager;
    /**
     * NormalizationUtils class provides normalization functionality for the adjacency and feature matrix
     */
    private final NormalizationUtils normalizationUtils;
    /**
     * The adjacency matrix of the original graph
     */
    private final NDArray adjacencyMatrix;
    /**
     * The feature matrix provided
     */
    private final NDArray featureMatrix;
    /**
     * The label matrix provided
     */
    private final NDArray labelMatrix;
    /**
     *A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the train phase.
     */
    private final String trainRange;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the validation phase.
     */
    private final String validationRange;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the test phase.
     */
    private final String testRange;
    /**
     * Whether to preprocess the adjaceny and feature matrix or not(default:true)
     */
    private final boolean preprocess;
    /**
     * The number of classes of the original dataset
     */
    private final int classesNo;
    /**
     * The size of the feature matrix over the first-axis
     */
    private final long featuresDimension;

    /**
     * private constructor used by the Builder class to provide instances of this class
     * @param builder Builder instance containing all the necessary information to invoke this constructor
     */
    private GraphDataset(Builder builder){
        this.device = builder.device;
        this.ndManager = NDManager.newBaseManager(this.device);
        this.normalizationUtils = new NormalizationUtils(this.ndManager);
        this.adjacencyMatrix = builder.adjacencyMatrix;
        this.featureMatrix = builder.featureMatrix;
        this.labelMatrix = builder.labelMatrix;
        this.trainRange = builder.trainRange;
        this.validationRange = builder.validationRange;
        this.testRange = builder.testRange;
        this.preprocess = builder.preprocess;
        this.classesNo = builder.numberOfClasses;
        this.featuresDimension = featureMatrix.size(builder.featureDimensionAxis);
    }

    /**
     * Builder class creates instances of this class by calling the private constructor
     */
    public static class Builder{
        private Device device = Device.defaultDevice();
        private NDArray adjacencyMatrix;
        private NDArray featureMatrix;
        private NDArray labelMatrix;
        private String trainRange;
        private String validationRange;
        private String testRange;
        private  boolean preprocess = true;
        private  int numberOfClasses;
        private int featureDimensionAxis = 1;

        public Builder adjacencyNDArray(final NDArray adjacencyMatrix){
            this.adjacencyMatrix = adjacencyMatrix;
            return this;
        }

        public Builder featureNDArray(final NDArray featureMatrix){
            this.featureMatrix = featureMatrix;
            return this;
        }

        public Builder labelNDArray(final NDArray labelMatrix){
            this.labelMatrix = labelMatrix;
            return this;
        }

        public Builder trainRange(final String trainRange){
            this.trainRange = trainRange;
            return this;
        }

        public Builder validationRange(final String validationRange){
            this.validationRange = validationRange;
            return this;
        }

        public Builder testRange(final String testRange){
            this.testRange = testRange;
            return this;
        }

        public Builder numberOfClasses(final int numberOfClasses){
            this.numberOfClasses = numberOfClasses;
            return this;
        }

        public Builder optDevice(final Device device){
            this.device = device;
            return this;
        }

        public Builder optPreprocess(final boolean preprocess){
            this.preprocess = preprocess;
            return this;
        }

        public Builder optFeatureDimensionAxis(final int featureDimensionAxis){
            this.featureDimensionAxis = featureDimensionAxis;
            return this;
        }

        public GraphDataset build(){
            return new GraphDataset(this);
        }

    }

    /*Getters*/
    public NDManager getNdManager() {
        return ndManager;
    }

    public NDArray getAdjacencyMatrix() {
        if(preprocess)
            return normalizationUtils.preprocessAdjacencyMatrix(adjacencyMatrix);
        return adjacencyMatrix;
    }

    public NDArray getFeatureMatrix() {
        if(preprocess)
            return normalizationUtils.preprocessFeatures(featureMatrix);
        return featureMatrix;
    }

    public NDArray getLabelMatrix() {
        return labelMatrix;
    }

    public String getTrainRange() {
        return trainRange;
    }

    public String getValidationRange() {
        return validationRange;
    }

    public String getTestRange() {
        return testRange;
    }

    public int getClassesNo() {
        return classesNo;
    }

    public long getFeaturesDimension() {
        return featuresDimension;
    }
}
