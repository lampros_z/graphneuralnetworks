package com.lzouloumis.nn;

import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.Shape;
import ai.djl.nn.AbstractBlock;
import ai.djl.training.ParameterStore;
import ai.djl.util.PairList;

/**
 * Simple block that applies the logSoftmax function over the horizontal axis
 */
public class LogSoftmaxBlock extends AbstractBlock {

    public LogSoftmaxBlock() {
        super((byte)2);
    }

    /**
     * This forward method just applies the softmax function and the calculates the logarithm row-wise.
     * @param parameterStore A map containing mirrors of a parameter on other devices
     * @param inputs Input from the previous blocks
     * @param training If training the model or not
     * @param params Additional model parameters
     * @return the result of the calculation of all the intermediate variables
     */
    @Override
    public NDList forwardInternal(ParameterStore parameterStore, NDList inputs, boolean training, PairList<String, Object> params) {
        NDList current = inputs;
        current = new NDList(current.singletonOrThrow().logSoftmax(1));
        return current;
    }

    /**
     * Returns the shape of this block
     * @param manager Manager used to create NDArrays
     * @param inputShapes An array of shapes given by the user
     * @return shape of this block
     */
    @Override
    public Shape[] getOutputShapes(NDManager manager, Shape[] inputShapes) {
        if(inputShapes == null)
            return new Shape[0];
        return new Shape[]{inputShapes[0]};
    }

}
