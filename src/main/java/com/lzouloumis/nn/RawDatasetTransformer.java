package com.lzouloumis.nn;

import ai.djl.Device;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.index.NDIndex;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import ai.djl.ndarray.types.SparseFormat;
import org.jgrapht.Graph;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * RawDatasetTransformer gets as input a Graph with its labels and features and transforms them to NDArrays that can be used later by the graph neural networks
 * @param <V> Vertex type of the graph
 * @param <E> Edge type of the graph
 * @param <F> Features type
 * @param <L> Labels type
 */
public class RawDatasetTransformer<V, E, F extends Buffer, L> {

    /**
     * The default device of the clients machine(usually it's set to gpu())
     */
    private static final Device DEVICE_DEFAULT_GPU = Device.defaultDevice();
    /**
     * The default manager provided by the djl library for processing NDArrays
     */
    private final NDManager manager;
    /**
     * The graph of the original dataset
     */
    private final Graph<V, E> graph;
    /**
     * A map containing mappings between each node and its features
     */
    private final Map<V, F> features;
    /**
     * A map containing mappings between each node and its labels
     */
    private final Map<V, L> labels;
    /**
     * All classes of the original dataset
     */
    private final  List<L> classes;
    /**
     * Maps vertices to ascending integer values
     */
    private final Map<Integer, V> vertexMap;
    /**
     * Fetches a vertex given its integer representation
     */
    private final Map<V, Integer> inverseVertexMap;
    /**
     * The final adjacency matrix that will be produced by the graph
     */
    private NDArray adjacencyMatrix;
    /**
     * The final feature matrix that will be produced by the features map
     */
    private NDArray featureMatrix;
    /**
     * The final label matrix that will be produced from the labels map
     */
    private final NDArray labelMatrix;
    /**
     * Helper field for mapping vertices to integer values
     */
    private int vertexIndex;


    /**
     * The only constructor that can be used to instantiate objects of this class
     * @param builder Instance of Builder class that containts all the necessary information to call this private constructor
     */
    private RawDatasetTransformer(Builder<V, E, F, L> builder) {
        Device device = builder.device;
        this.manager = NDManager.newBaseManager(device);
        this.graph = builder.graph;
        this.vertexMap = new HashMap<>();
        this.inverseVertexMap = new HashMap<>();
        this.features = builder.features;
        this.labels = builder.labels;
        this.classes = builder.classes;

        if(builder.adjacencyMatrix == null)
            this.adjacencyMatrix = buildAdjacencyMatrix();
        else
            this.adjacencyMatrix = builder.adjacencyMatrix;
        if (builder.featureMatrix == null)
            this.featureMatrix = buildFeatureMatrix();
        else
            this.featureMatrix = builder.featureMatrix;
        if(builder.labelMatrix == null)
            this.labelMatrix = buildLabelMatrix();
        else
            this.labelMatrix = builder.labelMatrix;

    }

    public NDArray getAdjacencyMatrix() {
        return adjacencyMatrix;
    }

    public NDArray getFeatureMatrix() {
        return featureMatrix;
    }

    public NDArray getLabelMatrix() {
        return labelMatrix;
    }


    /*public NDArray getEdgeIndex(boolean selfConnections){
        int numberOfEdges = graph.edgeSet().size();
        int[][] edgeIndexArray = new int[2][numberOfEdges];
        int edgeCounter = 0;
        if(selfConnections){
        }

        for (int sourceVertex = 0; sourceVertex < vertexMap.size(); sourceVertex++) {
            V vSource = vertexMap.get(sourceVertex);

            for (int targetVertex = 0; targetVertex < vertexMap.size(); targetVertex++) {
                V vTarget = vertexMap.get(targetVertex);

                if(graph.containsEdge(vSource, vTarget)){
                    edgeIndexArray[0][edgeCounter] = sourceVertex;
                    edgeIndexArray[1][edgeCounter++] = targetVertex;
                }

            }
        }

        return manager.create(edgeIndexArray).toType(DataType.INT32, false);
    }*/

    /**
     * Builder class is used to create instances of this class by calling the private constructor
     * @param <V> Vertex type of the graph
     * @param <E> Edge type of the graph
     * @param <F> Features type
     * @param <L> Labels type
     */
    public static class Builder<V, E, F extends Buffer, L> {

        private Device device = DEVICE_DEFAULT_GPU;
        private Graph<V, E> graph;
        private Map<V, F> features;
        private Map<V, L> labels;
        private List<L> classes;
        private NDArray adjacencyMatrix = null;
        private NDArray featureMatrix = null;
        private NDArray labelMatrix = null;

        public Builder<V, E, F, L> optDevice(final Device device){
            this.device = device;
            return this;
        }

        public Builder<V, E, F, L> graph(final Graph<V, E> graph){
            this.graph = graph;
            return this;
        }

        public Builder<V, E, F, L> featuresMap(final Map<V, F> features){
            this.features  = features;
            return this;
        }

        public Builder<V, E, F, L> labelsMap(final Map<V, L> labels){
            this.labels = labels;
            return this;
        }

        public Builder<V, E, F, L> classes(final List<L> classes){
            this.classes = classes;
            return this;
        }

        public Builder<V, E, F, L> optAdjacencyMatrix(final NDArray adjacencyMatrix){
            this.adjacencyMatrix = adjacencyMatrix;
            return this;
        }

        public Builder<V, E, F, L> optFeatureMatrix(final NDArray featureMatrix){
            this.featureMatrix = featureMatrix;
            return this;
        }

        public Builder<V, E, F, L> optLabelMatrix(final NDArray labelMatrix){
            this.labelMatrix = labelMatrix;
            return this;
        }

        public RawDatasetTransformer<V, E, F, L> build(){
            return new RawDatasetTransformer<>(this);
        }
    }

    /**
     * Maps vertices to integer values
     */
    private void buildVertexMap() {
        for(V vertex: graph.vertexSet()){
            vertexMap.put(vertexIndex, vertex);
            inverseVertexMap.put(vertex, vertexIndex);
            ++vertexIndex;
        }
    }


//    private NDArray buildAdjacencyMatrix(){
//        if(vertexMap.isEmpty())
//            buildVertexMap();
//        long length = graph.vertexSet().size();
//
//        if(length != vertexMap.size())
//            throw new RuntimeException("Fatal error!");
//        NDArray adjacencyMatrix = manager.zeros(new Shape(length, length), DataType.FLOAT32);
//
//        for (int i = 0; i < length; i++) {
//            V sourceVertex = vertexMap.get(i);
//            for (int j = 0; j < length; j++) {
//                V targetVertex = vertexMap.get(j);
//                if(graph.containsEdge(sourceVertex, targetVertex)) {
//                    adjacencyMatrix.set(new NDIndex(i + ", " + j), 1d);
//                }
//            }
//        }
//        return adjacencyMatrix.toSparse(SparseFormat.ROW_SPARSE);
//    }

    /**
     * Build the adjacency matrix from the input graph
     * @return The adjacency NDArray in CSR format
     */
    private NDArray buildAdjacencyMatrix(){
        buildVertexMap();
        int vertices = graph.vertexSet().size();
        int edges = graph.edgeSet().size();
        long[] columnIndices;
        if(graph.getType().isUndirected())
            columnIndices = new long[edges*2];
        else
            columnIndices = new long[edges];
        long[] rowIndices = new long[vertices+1];
        int rowIndixesPosition = 0;
        int columnIndicesPosition = 0;
        long cumulativeSum = 0;
        rowIndices[rowIndixesPosition++] = 0;
        for(int vertex = 0; vertex < vertexMap.size(); vertex++){
            V vertexNode = vertexMap.get(vertex);
            for (int targetVertex = 0; targetVertex < vertexMap.size(); targetVertex++) {
                V vertexNode2 = vertexMap.get(targetVertex);
                if(graph.containsEdge(vertexNode, vertexNode2)){
                    columnIndices[columnIndicesPosition++] = targetVertex;
                    cumulativeSum++;
                }
            }
            rowIndices[rowIndixesPosition++] = cumulativeSum;
        }

        byte[] values;
        if (graph.getType().isUndirected())
            values = new byte[edges*2];
        else
            values = new byte[edges];

        Arrays.fill(values, (byte)1);
        ByteBuffer byteBuffer = ByteBuffer.wrap(values);
        Shape shape = new Shape(vertices, vertices);
        NDArray adjacencyMatrix  = manager.createCSR(byteBuffer, rowIndices, columnIndices, shape);
        return adjacencyMatrix;
    }

    /**
     * Builds the feature matrix from the input features map
     * @return The features NDArray in CSR format
     */
    private NDArray buildFeatureMatrix(){

        long dimensionX = graph.vertexSet().size();
        long dimensionY = features.values().stream().findFirst().get().capacity();
        Shape shape = new Shape(dimensionX, dimensionY);
        NDArray featureMatrix = manager.create(shape);
        if( vertexMap.isEmpty() )
            buildVertexMap();
        for (int vertex = 0; vertex < vertexMap.size(); vertex++) {
            V vVertex = vertexMap.get(vertex);
            F feature = features.get(vVertex);
            NDArray featureValues = manager.create(feature, new Shape(feature.capacity()));
            featureMatrix.set(new NDIndex(vertex), featureValues.toType(DataType.FLOAT32, false));
        }
        return featureMatrix.toSparse(SparseFormat.ROW_SPARSE);

    }

    /**
     * Builds the label matrix from the input labels map
     * @return The labels NDArray
     */
    private NDArray buildLabelMatrix(){
        if(vertexMap.isEmpty())
            buildVertexMap();
        long dimensionX = graph.vertexSet().size();
        Shape shape = new Shape(dimensionX);
        NDArray labelMatrix = manager.create(shape);
        Map<L, Integer> classesMap = new HashMap<>();
        int classIndex = 0;
        for (L label : classes){
            classesMap.put(label, classIndex++);
        }
        if( vertexMap.isEmpty() )
            buildVertexMap();
        for(int vertex = 0; vertex < vertexMap.size(); vertex++){
            V vVertex = vertexMap.get(vertex);
            L label = labels.get(vVertex);
            labelMatrix.set(new NDIndex(vertex), classesMap.get(label));
        }
        return labelMatrix;
    }

}
