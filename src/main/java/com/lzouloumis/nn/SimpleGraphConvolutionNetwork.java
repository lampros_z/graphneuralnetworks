package com.lzouloumis.nn;

import ai.djl.Device;
import ai.djl.MalformedModelException;
import ai.djl.Model;
import ai.djl.ndarray.NDArray;
import ai.djl.ndarray.NDList;
import ai.djl.ndarray.NDManager;
import ai.djl.ndarray.types.DataType;
import ai.djl.ndarray.types.Shape;
import ai.djl.nn.Block;
import ai.djl.nn.core.Linear;
import ai.djl.training.DefaultTrainingConfig;
import ai.djl.training.GradientCollector;
import ai.djl.training.Trainer;
import ai.djl.training.loss.Loss;
import ai.djl.training.optimizer.Optimizer;
import ai.djl.training.tracker.Tracker;

import java.io.*;

/**
 * SimpleGraphConvolutionNetwork class, as its name suggests implements the Simple Graph Convolution Layer as specified in: https://arxiv.org/abs/1902.07153
 * It contains all the necessary information and methods to build and train a simple graph convolution layer given a graph dataset as input.
 */
public class SimpleGraphConvolutionNetwork implements AutoCloseable, ModelPersistable {
    /**
     * The manager provided by the djl library(https://djl.ai/) for managing NDArrays
     */
    private final NDManager manager;
    /**
     * Constant holding the name of the model
     */
    public static final String MODEL_NAME = "GRAPH_CONVOLUTION_NETWORK";

    /*Hyperparameter default values*/
    /**
     * Weight decay hyperparameter
     */
    public static final double WEIGHT_DECAY = 5e-6;
    /**
     * Learning rate hyperparameter
     */
    public static final double LEARNING_RATE = 0.2;
    /**
     * The default number of epochs
     */
    public static final int EPOCHS = 100;
    /**
     * The degree of the augmented adjacency matrix
     */
    public static final int DEGREE = 2;

    /*Hyperparameter class fields*/
    private  double weightDecay;
    private  double learningRate;
    private  int epochs;
    private int degree;

    /**
     * The default Model interface provided by the djl library
     */
    private Model model;
    /**
     * The default Block interface provided by the djl library.
     * This field holds the linear block that will be used by the SGC layer
     */
    private Block linearBlock;
    /**
     * The adjacency matrix of the input graph
     */
    private  NDArray adjacency = null;
    /**
     * Number of classes that the graph dataset has
     */
    private  int numberOfClasses = -1;
    /**
     * Feature vector of each node
     */
    private NDArray features;
    /**
     * A vector containing the labels of each node
     */
    private  NDArray labels = null;
    /**
     * The loss function of the model(SoftmaxCrossEntropyLoss for node classification tasks)
     */
    private Loss loss;
    /**
     * The default trainer interface provided by the djl library for providing a session for model training
     */
    private Trainer trainer;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the training phase.
     */
    private String idxTrain = null;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the validation phase.
     */
    private String idxVal = null;
    /**
     * A string of the form "startIndex:endIndex" which is used to fetch specific nodes from the whole graph dataset in order to be used at the testing phase.
     */
    private String idxTest = null;
    /**
     * An array for storing the train accuracy value of each epoch
     */
    private double[] trainAccuracy;
    /**
     * An array for storing the train loss value of each epoch
     */
    private double[] trainLoss;
    /**
     * An array for storing the validation accuracy value of each epoch
     */
    private  double[] validationAccuracy;

    /**
     * Default constructor used from the builder class to provide instances of this class
     * @param builder A builder object containing the initialization values
     */
    private SimpleGraphConvolutionNetwork(Builder builder){
        GraphDataset graphDataset = builder.graphDataset;
        manager = NDManager.newBaseManager(builder.device);
        weightDecay = builder.weightDecay;
        learningRate = builder.learningRate;
        epochs = builder.epochs;
        degree = builder.degree;
        if(graphDataset != null) {
            this.adjacency = graphDataset.getAdjacencyMatrix();
            this.features = graphDataset.getFeatureMatrix();
            this.labels = graphDataset.getLabelMatrix();
            this.idxTrain = graphDataset.getTrainRange();
            this.idxVal = graphDataset.getValidationRange();
            this.idxTest = graphDataset.getTestRange();
            this.numberOfClasses = graphDataset.getClassesNo();
            buildModel();
        }
    }

    /**
     * Builder class is used to define the internal state of SimpleGraphConvolution Objects and then
     * initialize them by using the private constructor.
     */
    public static class Builder{
        /**
         * Field holding the required graph dataset data
         */
        private GraphDataset graphDataset;
        /**
         * The device which will be processing the NDArray of this class
         */
        private Device device;
        /*Model hyperparameters*/
        private int epochs = EPOCHS;
        private double weightDecay = WEIGHT_DECAY;
        private double learningRate = LEARNING_RATE;
        private int degree = DEGREE;

        public Builder graphDataset(final GraphDataset graphDataset){
            this.graphDataset = graphDataset;
            return this;
        }

        public Builder optDevice(final Device device){
            this.device = device;
            return this;
        }

        public Builder optEpochs(final int epochs){
            this.epochs = epochs;
            return this;
        }


        public Builder optWeightDecay(final double weightDecay ){
            this.weightDecay = weightDecay;
            return this;
        }

        public Builder optLearningRate(final double learningRate ){
            this.learningRate = learningRate;
            return this;
        }

        public Builder optDegree(final int degree){
            this.degree = degree;
            return this;
        }

        public SimpleGraphConvolutionNetwork build(){
            return new SimpleGraphConvolutionNetwork(this);
        }

    }

    /**
     * Method responsible for creating the simple graph convolution model
     */
    private void buildModel() {
        model = Model.newInstance(MODEL_NAME);
        model.setDataType(DataType.FLOAT32);
        linearBlock = Linear.builder().setUnits(numberOfClasses).optBias(false).build();
        model.setBlock(linearBlock);

        /*Logit softmax cross entropy loss*/
        loss = Loss.softmaxCrossEntropyLoss();
        Tracker lrt = Tracker.fixed((float) learningRate);
        Optimizer adam = Optimizer.adam().optLearningRateTracker(lrt).optWeightDecays((float) weightDecay).build();
        DefaultTrainingConfig config = new DefaultTrainingConfig(loss).optOptimizer(adam).optDevices(new Device[]{manager.getDevice()});
        trainer = model.newTrainer(config);
    }

    /**
     * Train method is used at the train phase of the model
     */
    public void train() {
        trainAccuracy = new double[epochs];
        trainLoss = new double[epochs];
        validationAccuracy = new double[epochs];

        for (int i = 0; i < degree; i++) {
            features = adjacency.matMul(features);
        }

        double accuracyVal;
        NDArray tempX;
        NDArray tempy;
        NDList yHat;
        NDArray lossValue;

        for (int epoch = 1; epoch <= epochs; epoch++) {
            System.out.println("Running epoch " + epoch + "...... ");
            try(NDManager localManager = trainer.getManager().newSubManager(manager.getDevice())) {
                tempX = localManager.create(features.getShape());
                tempy = localManager.create(labels.getShape());
                features.copyTo(tempX);
                labels.copyTo(tempy);
                try (GradientCollector gradientCollector = trainer.newGradientCollector()) {

                    yHat = trainer.forward(new NDList(tempX)); // net function call
                    lossValue = loss.evaluate(new NDList(tempy.get(idxTrain)), new NDList(yHat.head().get(idxTrain)));
                    accuracyVal = accuracy(new NDList(tempy.get(idxTrain)), new NDList(yHat.head().get(idxTrain)));
                    trainAccuracy[epoch - 1] = accuracyVal;
                    gradientCollector.backward(lossValue);
                    trainLoss[epoch - 1] = lossValue.reshape(new Shape(1)).getFloat(0);
                    System.out.println("\nloss: " + lossValue.reshape(new Shape(1)).getFloat(0) + ", acc: " + accuracyVal);
                    trainer.step();

                    yHat.close();
                    lossValue.close();
                }

                //Validation
                validation(idxVal, new NDList(tempX, tempy), epoch);

            }

        }
        test();
    }

    /**
     * Test method is used at the testing phase of the model
     */
    private void test() {
        if(features == null || labels == null || idxTest == null)
            return;
        NDList yHat = trainer.evaluate(new NDList(features)); // predict class
        double valAcc = accuracy(new NDList(labels.get(idxTest)), new NDList(yHat.head().get(idxTest)));
        System.out.println("Test Validation accuracy: " + valAcc);
    }


    /**
     * Closes all closable resources
     */
    @Override
    public void close(){
        trainer.close();
        features.close();
        labels.close();
    }


    @Override
    public void saveModel(String filename){
        try(DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(filename + ARCHITECTURE_FILE_EXTENSION))){
            /*Save model architecture*/
            dataOutputStream.writeBoolean(false);
            dataOutputStream.writeInt(numberOfClasses);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
        try(DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(filename))){
            /*Save model parameters*/
            linearBlock.saveParameters(dataOutputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadModel(String filename){
        /*Architecture variables*/
        boolean bias = false;
        int units = -1;
        /* Load architecture */
        try(DataInputStream dataInputStream = new DataInputStream(new FileInputStream(filename+ARCHITECTURE_FILE_EXTENSION))) {
            bias = dataInputStream.readBoolean();
            units = dataInputStream.readInt();
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println(e.getMessage());
            e.printStackTrace();
        }
        linearBlock = Linear.builder().setUnits(units).optBias(bias).build();
        /* Load model parameters */
        try(DataInputStream dataInputStream = new DataInputStream(new FileInputStream(filename))){
            linearBlock.loadParameters(manager, dataInputStream);
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        } catch (MalformedModelException e) {
            System.err.println(e.getMessage());
        }
        /* Create model */
        model = Model.newInstance(MODEL_NAME);
        model.setDataType(DataType.FLOAT32);
        model.setBlock(linearBlock);
    }

    /*private NDList decodeOneHot(NDList labels){
        NDArray label = labels.head();
        NDArray decodedArray = label.argMax(1).toType(DataType.FLOAT32, false);
        return new NDList(decodedArray);
    }*/

    public Model getModel() {
        return model;
    }

    public double[] getTrainAccuracy() {
        return trainAccuracy;
    }

    public double[] getTrainLoss() {
        return trainLoss;
    }

    public double[] getValidationAccuracy() {
        return validationAccuracy;
    }

    /**
     * Method used at the validation phase of the model to calculate the validation accuracy at the specified epoch
     * @param range A string formatted as "startIndex:endIndex" which fetches specific nodes from the whole graph dataset to be tested during the validation phase
     * @param params An NDList containing at the first index the features and at the second index the labels of the initial dataset
     * @param epoch The current training epoch
     */
    private void validation(String range, NDList params, int epoch) {
        NDList yHat = trainer.evaluate(new NDList(params.get(0))); // predict class
        double valAcc = accuracy(new NDList(params.get(1).get(range)), new NDList(yHat.get(0).get(range)));
        validationAccuracy[epoch-1] = valAcc;
        System.out.println("Validation accuracy: " + valAcc);
    }

    /**
     * Method that given the label matrix and the prediction matrix of the model calculates the accuracy
     * @param labels The label matrix
     * @param predictions The matrix with the predictions
     * @return The accuracy of the predictions
     */
    private double accuracy(NDList labels, NDList predictions){
        NDArray label = labels.head();
        NDArray prediction = predictions.head();
        checkShapes(prediction, label);
        /* Multi-class, sparse label */
        NDArray predictionReduced = prediction.argMax(1);
        long total = label.size();
        try (NDArray nd = label.toType(DataType.INT64, true)) {
            NDArray correct = predictionReduced.toType(DataType.INT64, false).eq(nd).countNonzero().reshape(new Shape(1));
            return  (double)correct.getLong(0)/total;
        }
    }

    /**
     * Checks if the number of nodes of the predictions matrix and the labels matrix is equal
     * @param predictions The predictions matrix
     * @param labels The labels matrix
     */
    private void checkShapes(NDArray predictions, NDArray labels){
        if (labels.getShape().get(0) != predictions.getShape().get(0)) {
            throw new IllegalArgumentException(
                    "The size of labels("
                            + labels.size()
                            + ") does not match that of predictions("
                            + predictions.size()
                            + ")");
        }
    }

}
