package com.lzouloumis.demos.data;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.FloatBuffer;
import java.util.*;

public class PubmedDataLoader {
    public static final String CITES_FILE_URL = "https://lzouloumis-file-server.herokuapp.com/pubmed-cites";
    public static final String CONTENTS_FILE_URL = "https://lzouloumis-file-server.herokuapp.com/pubmed-paper";
    public static final String CITES_FILE = "src/main/resources/Pubmed-Diabetes.DIRECTED.cites.tab";
    public static final String CONTENT_FILE = "src/main/resources/Pubmed-Diabetes.NODE.paper.tab";
    public static final String TRAIN_RANGE = "0:60";
    public static final String VALIDATION_RANGE = "100:600";
    public static final String TEST_RANGE = "600:1600";

    private Graph<Long, DefaultEdge> graph;
    private List<String> classes;
    private Map<Long, FloatBuffer> featuresMap;
    private Map<Long, String> labelsMap;

    private Map<Integer, String> featuresDictionary;
    private Map<String, Integer> inverseFeatureDictionary;
    private int featureCount = 0;

    public PubmedDataLoader() {
        downloadFilesIfNecessary();
        graph = new SimpleGraph<>(DefaultEdge.class);
        classes = new ArrayList<>();
        featuresMap = new HashMap<>();
        labelsMap = new HashMap<>();
        featuresDictionary = new HashMap<>();
        inverseFeatureDictionary = new HashMap<>();
        buildClasses();
        parseContentFile(CONTENT_FILE);
        parseCitesFile(CITES_FILE);
    }

    private void downloadFilesIfNecessary(){
        FileDownloader fileDownloader = new FileDownloader();
        fileDownloader.downloadTextFile(CITES_FILE_URL, CITES_FILE);
        fileDownloader.downloadTextFile(CONTENTS_FILE_URL, CONTENT_FILE);
    }

    private void buildClasses(){
        classes.add("1");
        classes.add("2");
        classes.add("3");
    }

    private void parseCitesFile(String filepath){
        String inputLine = "";
        try(BufferedReader reader = new BufferedReader(new FileReader(filepath))){
            /*Ignore first two lines*/
            reader.readLine();
            reader.readLine();
            while( (inputLine = reader.readLine()) != null ){

                String[] tokens = inputLine.split("\t");
                Long target = Long.valueOf(tokens[1].split(":")[1]);
                Long source = Long.valueOf(tokens[3].split(":")[1]);
                if(!graph.containsVertex(source)) {
                    continue;
                }
                if(!graph.containsVertex(target)){
                    continue;
                }
                if(target.equals(source))
                    continue;
                graph.addEdge(source, target);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private void parseContentFile(String filepath) {
        String inputLine = "";
        try(BufferedReader reader = new BufferedReader(new FileReader(filepath))){
            reader.readLine();

            String[] features = reader.readLine().split("\t");
            Iterator<String> featuresIterator = Arrays.stream(features).iterator();
            featuresIterator.next();
            while (featuresIterator.hasNext()){
                String feature = featuresIterator.next();
                String[] tokens = feature.split(":");
                if(tokens[0].equals("numeric")) {
                    featuresDictionary.put(featureCount, tokens[1]);
                    inverseFeatureDictionary.put(tokens[1], featureCount);
                    ++featureCount;
                }
            }

            while( (inputLine = reader.readLine()) != null ){
                String[] tokens = inputLine.split("\t");
                int start = 0;
                int end = tokens.length-1;

                Long vertex = Long.valueOf(tokens[start]);
                if(vertex == null)
                    throw new NullPointerException("Preprocessing");
                String label = tokens[start+1].split("=")[1];
                if (label == null)
                    throw new NullPointerException("Preprocessing");
                float[] featuresVector = new float[featuresDictionary.size()];
                Arrays.fill(featuresVector,0);
                for (int i = start+2; i <= end; i++) {
                    String token = tokens[i];
                    String tag = token.split("=")[0];
                    if(tag.equals("summary"))
                        break;
                    String value = token.split("=")[1];
                    int index = inverseFeatureDictionary.get(tag);
                    featuresVector[index] = Float.valueOf(value);
                }
                FloatBuffer featuresDoubleBuffer = FloatBuffer.wrap(featuresVector);
                graph.addVertex(vertex);
                featuresMap.put(vertex, featuresDoubleBuffer);
                labelsMap.put(vertex, label);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public Graph<Long, DefaultEdge> graph() {
        return graph;
    }

    public Map<Long, FloatBuffer> featuresMap() {
        return featuresMap;
    }

    public Map<Long, String> labelsMap() {
        return labelsMap;
    }

    public List<String> classes() {
        return classes;
    }

}
