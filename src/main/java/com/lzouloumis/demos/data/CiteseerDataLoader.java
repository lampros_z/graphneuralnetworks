package com.lzouloumis.demos.data;

import org.apache.commons.lang3.ArrayUtils;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class CiteseerDataLoader {
    public static final String CITES_FILE_URL = "https://lzouloumis-file-server.herokuapp.com/citeseer-cites";
    public static final String CONTENTS_FILE_URL = "https://lzouloumis-file-server.herokuapp.com/citeseer-content";
    public static final String CITES_FILE = "src/main/resources/citeseer.cites";
    public static final String CONTENT_FILE = "src/main/resources/citeseer.content";

    public static final String TRAIN_RANGE = "2900:3020";
    public static final String VALIDATION_RANGE = "2000:2500";
    public static final String TEST_RANGE = "1000:2000";

    private Graph<String, DefaultEdge> graph;
    private List<String> classes;
    private Map<String, ByteBuffer> featuresMap;
    private Map<String, String> labelsMap;

    public CiteseerDataLoader() {
        downloadFilesIfNecessary();
        graph = new SimpleGraph<>(DefaultEdge.class);
        classes = new ArrayList<>();
        featuresMap = new HashMap<>();
        labelsMap = new HashMap<>();
        buildClasses();
        parseContentFile(CONTENT_FILE);
        parseCitesFile(CITES_FILE);
    }

    private void downloadFilesIfNecessary(){
        FileDownloader fileDownloader = new FileDownloader();
        fileDownloader.downloadTextFile(CITES_FILE_URL, CITES_FILE);
        fileDownloader.downloadTextFile(CONTENTS_FILE_URL, CONTENT_FILE);
    }

    private void buildClasses(){
        classes.add("Agents");
        classes.add("AI");
        classes.add("DB");
        classes.add("IR");
        classes.add("ML");
        classes.add("HCI");
    }

    private void parseCitesFile(String filepath){
        String inputLine = "";
        try(BufferedReader reader = new BufferedReader(new FileReader(filepath))){
            while( (inputLine = reader.readLine()) != null ){
                String[] tokens = inputLine.split("\t");
                String target = tokens[0];
                String source = tokens[1];
                if(!graph.containsVertex(source)) {
                    continue;
                }
                if(!graph.containsVertex(target)){
                    continue;
                }
                if(source.equals(target))
                    continue;
                graph.addEdge(source, target);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private void parseContentFile(String filepath) {
        String inputLine = "";
        try(BufferedReader reader = new BufferedReader(new FileReader(filepath))){
            while( (inputLine = reader.readLine()) != null ){
                String[] tokens = inputLine.split("\t");
                int start = 0;
                int end = tokens.length-1;
                String vertex = tokens[start];
                Byte[] features = IntStream.range(start+1,end).mapToObj(i -> Byte.parseByte(tokens[i])).toArray(Byte[]::new);
                byte[] unboxedFeatures = ArrayUtils.toPrimitive(features);
                ByteBuffer featuresByteBuffer = ByteBuffer.wrap(unboxedFeatures);
                String label = tokens[end];
                graph.addVertex(vertex);
                featuresMap.put(vertex, featuresByteBuffer);
                labelsMap.put(vertex, label);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public Graph<String, DefaultEdge> graph() {
        return graph;
    }

    public Map<String, ByteBuffer> featuresMap() {
        return featuresMap;
    }

    public Map<String, String> labelsMap() {
        return labelsMap;
    }

    public List<String> classes() {
        return classes;
    }
}
