package com.lzouloumis.demos.data;

import org.apache.commons.lang3.ArrayUtils;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.SimpleGraph;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class CoraDataLoader {
    public static final String CITES_FILE_URL = "https://lzouloumis-file-server.herokuapp.com/cora-cites";
    public static final String CONTENTS_FILE_URL = "https://lzouloumis-file-server.herokuapp.com/cora-content";
    public static final String CITES_FILE = "src/main/resources/cora.cites";
    public static final String CONTENT_FILE = "src/main/resources/cora.content";
    public static final String TRAIN_RANGE = "0:140";
    public static final String VALIDATION_RANGE = "200:500";
    public static final String TEST_RANGE = "500:1500";

    private Graph<Integer, DefaultEdge> graph;
    private List<String> classes;
    private Map<Integer, ByteBuffer> featuresMap;
    private Map<Integer, String> labelsMap;

    public CoraDataLoader() {
        downloadFilesIfNecessary();
        graph = new SimpleGraph<>(DefaultEdge.class);
        classes = new ArrayList<>();
        featuresMap = new HashMap<>();
        labelsMap = new HashMap<>();
        buildClasses();
        parseContentFile(CONTENT_FILE);
        parseCitesFile(CITES_FILE);
    }

    private void downloadFilesIfNecessary(){
        FileDownloader fileDownloader = new FileDownloader();
        fileDownloader.downloadTextFile(CITES_FILE_URL, CITES_FILE);
        fileDownloader.downloadTextFile(CONTENTS_FILE_URL, CONTENT_FILE);
    }

    private void buildClasses(){
        classes.add("Neural_Networks");
        classes.add("Rule_Learning");
        classes.add("Reinforcement_Learning");
        classes.add("Probabilistic_Methods");
        classes.add("Theory");
        classes.add("Genetic_Algorithms");
        classes.add("Case_Based");
    }

    private void parseCitesFile(String filepath){
        String inputLine = "";
        try(BufferedReader reader = new BufferedReader(new FileReader(filepath))){
            while( (inputLine = reader.readLine()) != null ){
                String[] tokens = inputLine.split("\t");
                Integer target = Integer.valueOf(tokens[0]);
                Integer source = Integer.valueOf(tokens[1]);
                if(!graph.containsVertex(source)) {
                    graph.addVertex(source);
                }
                if(!graph.containsVertex(target)){
                    graph.addVertex(target);
                }
                graph.addEdge(source, target);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    private void parseContentFile(String filepath) {
        String inputLine = "";
        try(BufferedReader reader = new BufferedReader(new FileReader(filepath))){
            while( (inputLine = reader.readLine()) != null ){
                String[] tokens = inputLine.split("\t");
                int start = 0;
                int end = tokens.length-1;
                Integer vertex = Integer.valueOf(tokens[start]);
                Byte[] features = IntStream.range(start+1,end).mapToObj(i -> Byte.parseByte(tokens[i])).toArray(Byte[]::new);
                byte[] unboxedFeatures = ArrayUtils.toPrimitive(features);
                ByteBuffer featuresByteBuffer = ByteBuffer.wrap(unboxedFeatures);
                String label = tokens[end];
                graph.addVertex(vertex);
                featuresMap.put(vertex, featuresByteBuffer);
                labelsMap.put(vertex, label);
            }
        } catch (FileNotFoundException e) {
            System.err.println(e.getMessage());
        } catch (IOException e) {
            System.err.println(e.getMessage());
        }
    }

    public Graph<Integer, DefaultEdge> graph() {
        return graph;
    }

    public Map<Integer, ByteBuffer> featuresMap() {
        return featuresMap;
    }

    public Map<Integer, String> labelsMap() {
        return labelsMap;
    }

    public List<String> classes() {
        return classes;
    }

}
