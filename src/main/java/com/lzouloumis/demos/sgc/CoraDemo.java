package com.lzouloumis.demos.sgc;

import com.lzouloumis.demos.data.CoraDataLoader;
import com.lzouloumis.nn.GraphDataset;
import com.lzouloumis.nn.RawDatasetTransformer;
import com.lzouloumis.nn.SimpleGraphConvolutionNetwork;
import org.jgrapht.graph.DefaultEdge;


import java.nio.ByteBuffer;

public class CoraDemo {

    public static void main(String... args) {
        System.out.println("### Loading data ###");
        CoraDataLoader coraDataLoader = new CoraDataLoader();
        System.out.println("### Finished loading data ###");
        System.out.println("### Creating dataset ###");

        RawDatasetTransformer<Integer, DefaultEdge, ByteBuffer, String> rawDatasetTransformer = new RawDatasetTransformer.Builder<Integer, DefaultEdge, ByteBuffer, String>()
                .graph(coraDataLoader.graph())
                .featuresMap(coraDataLoader.featuresMap())
                .labelsMap(coraDataLoader.labelsMap())
                .classes(coraDataLoader.classes())
                .build();

        GraphDataset graphDataset = new GraphDataset.Builder()
                .adjacencyNDArray(rawDatasetTransformer.getAdjacencyMatrix())
                .featureNDArray(rawDatasetTransformer.getFeatureMatrix())
                .labelNDArray(rawDatasetTransformer.getLabelMatrix())
                .trainRange(CoraDataLoader.TRAIN_RANGE)
                .validationRange(CoraDataLoader.VALIDATION_RANGE)
                .testRange(CoraDataLoader.TEST_RANGE)
                .numberOfClasses(coraDataLoader.classes().size())
                .build();


        System.out.println("### Finished creating dataset ###");

        SimpleGraphConvolutionNetwork sgc  = new SimpleGraphConvolutionNetwork.Builder()
                .graphDataset(graphDataset)
                .build();

            sgc.train();

    }
}
