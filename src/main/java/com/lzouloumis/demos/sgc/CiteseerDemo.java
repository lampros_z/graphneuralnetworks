package com.lzouloumis.demos.sgc;

import com.lzouloumis.demos.data.CiteseerDataLoader;
import com.lzouloumis.nn.GraphDataset;
import com.lzouloumis.nn.RawDatasetTransformer;
import com.lzouloumis.nn.SimpleGraphConvolutionNetwork;
import org.jgrapht.graph.DefaultEdge;

import java.nio.ByteBuffer;

public class CiteseerDemo {
    public static void main(String... args) {
        System.out.println("### Loading data ###");
        CiteseerDataLoader citeseerDataLoader = new CiteseerDataLoader();
        System.out.println("### Finished loading data ###");
        System.out.println("### Creating dataset ###");

        RawDatasetTransformer<String, DefaultEdge, ByteBuffer, String> rawDatasetTransformer = new RawDatasetTransformer.Builder<String, DefaultEdge, ByteBuffer, String>()
                .graph(citeseerDataLoader.graph())
                .featuresMap(citeseerDataLoader.featuresMap())
                .labelsMap(citeseerDataLoader.labelsMap())
                .classes(citeseerDataLoader.classes())
                .build();

        GraphDataset graphDataset = new GraphDataset.Builder()
                .adjacencyNDArray(rawDatasetTransformer.getAdjacencyMatrix())
                .featureNDArray(rawDatasetTransformer.getFeatureMatrix())
                .labelNDArray(rawDatasetTransformer.getLabelMatrix())
                .trainRange(CiteseerDataLoader.TRAIN_RANGE)
                .validationRange(CiteseerDataLoader.VALIDATION_RANGE)
                .testRange(CiteseerDataLoader.TEST_RANGE)
                .numberOfClasses(citeseerDataLoader.classes().size())
                .build();

        System.out.println("### Finished creating dataset ###");

        SimpleGraphConvolutionNetwork sgc  = new SimpleGraphConvolutionNetwork.Builder()
                .graphDataset(graphDataset)
                .build();

        sgc.train();

    }
}
