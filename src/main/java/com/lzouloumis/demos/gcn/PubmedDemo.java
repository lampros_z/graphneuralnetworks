package com.lzouloumis.demos.gcn;

import ai.djl.Device;
import com.lzouloumis.demos.data.PubmedDataLoader;
import com.lzouloumis.nn.GraphConvolutionNetwork;
import com.lzouloumis.nn.GraphDataset;
import com.lzouloumis.nn.RawDatasetTransformer;
import org.jgrapht.graph.DefaultEdge;

import java.nio.FloatBuffer;

public class PubmedDemo {
    public static void main(String... args) {
        System.out.println("### Loading data ###");
        PubmedDataLoader pubmedDataLoader = new PubmedDataLoader();
        System.out.println("### Finished loading data ###");
        System.out.println("### Creating dataset ###");
        RawDatasetTransformer<Long, DefaultEdge, FloatBuffer, String> rawDatasetTransformer = new RawDatasetTransformer.Builder<Long, DefaultEdge, FloatBuffer, String>()
                .graph(pubmedDataLoader.graph())
                .optDevice(Device.cpu())
                .featuresMap(pubmedDataLoader.featuresMap())
                .labelsMap(pubmedDataLoader.labelsMap())
                .classes(pubmedDataLoader.classes())
                .build();


        GraphDataset graphDataset = new GraphDataset.Builder()
                .adjacencyNDArray(rawDatasetTransformer.getAdjacencyMatrix())
                .featureNDArray(rawDatasetTransformer.getFeatureMatrix())
                .labelNDArray(rawDatasetTransformer.getLabelMatrix())
                .trainRange(PubmedDataLoader.TRAIN_RANGE)
                .validationRange(PubmedDataLoader.VALIDATION_RANGE)
                .testRange(PubmedDataLoader.TEST_RANGE)
                .numberOfClasses(pubmedDataLoader.classes().size())
                .build();

        System.out.println("### Finished creating dataset ###");

        GraphConvolutionNetwork gcn  = new GraphConvolutionNetwork.Builder()
                .optDevice(Device.cpu())
                .graphDataset(graphDataset)
                .build();

            gcn.train();

            gcn.test();
    }
}
