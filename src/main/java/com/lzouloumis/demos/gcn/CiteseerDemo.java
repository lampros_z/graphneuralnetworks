package com.lzouloumis.demos.gcn;

import ai.djl.Device;
import com.lzouloumis.demos.data.CiteseerDataLoader;
import com.lzouloumis.nn.GraphConvolutionNetwork;
import com.lzouloumis.nn.GraphDataset;
import com.lzouloumis.nn.RawDatasetTransformer;
import org.jgrapht.graph.DefaultEdge;

import java.nio.ByteBuffer;

public class CiteseerDemo {
    public static void main(String... args) {
        System.out.println("### Loading data ###");
        CiteseerDataLoader citeseerDataLoader = new CiteseerDataLoader();
        System.out.println("### Finished loading data ###");
        System.out.println("### Creating dataset ###");

        RawDatasetTransformer<String, DefaultEdge, ByteBuffer, String> rawDatasetTransformer = new RawDatasetTransformer.Builder<String, DefaultEdge, ByteBuffer, String>()
                .optDevice(Device.cpu())
                .graph(citeseerDataLoader.graph())
                .featuresMap(citeseerDataLoader.featuresMap())
                .labelsMap(citeseerDataLoader.labelsMap())
                .classes(citeseerDataLoader.classes())
                .build();
        System.out.println("### Finished creating dataset ###");


        GraphDataset graphDataset = new GraphDataset.Builder()
                .adjacencyNDArray(rawDatasetTransformer.getAdjacencyMatrix())
                .featureNDArray(rawDatasetTransformer.getFeatureMatrix())
                .labelNDArray(rawDatasetTransformer.getLabelMatrix())
                .trainRange(CiteseerDataLoader.TRAIN_RANGE)
                .validationRange(CiteseerDataLoader.VALIDATION_RANGE)
                .testRange(CiteseerDataLoader.TEST_RANGE)
                .numberOfClasses(citeseerDataLoader.classes().size())
                .build();

        GraphConvolutionNetwork gcn  = new GraphConvolutionNetwork
                .Builder()
                .optDevice(Device.cpu())
                .graphDataset(graphDataset)
                .build();


            gcn.train();

            gcn.test();
    }
}
